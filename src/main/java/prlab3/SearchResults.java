package prlab3;

        import java.util.HashMap;

public class SearchResults {
    // Container class for search results encapsulates relevant headers and JSON data

    HashMap<String, String> relevantHeaders;
    String jsonResponse;
    SearchResults(HashMap<String, String> headers, String json) {
        relevantHeaders = headers;
        jsonResponse = json;

    }
}
