package prlab3;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Pattern;

public class App {

// ***********************************************
// *** Update or verify the following values. ***
// **********************************************

    // Replace the subscriptionKey string value with your valid subscription key.
    static String subscriptionKey = "55963624daca4139814e8f7469e7d617";

    // Verify the endpoint URI.  At this writing, only one endpoint is used for Bing
    // search APIs.  In the future, regional endpoints may be available.  If you
    // encounter unexpected authorization errors, double-check this value against
    // the endpoint for your Bing Web search instance in your Azure dashboard.
    static String host = "https://api.cognitive.microsoft.com";
    static String path = "/bing/v7.0/search";

    private static final int max_depth = 2;
    private static int count = 0;
    private static ArrayList<String> visited = new ArrayList<String>();

    static String searchTerm = "Dublin";

    public static SearchResults SearchWeb (String searchQuery) throws Exception {
        // construct URL of search request (endpoint + query string)
        URL url = new URL(host + path + "?q=" +  URLEncoder.encode(searchQuery, "UTF-8"));
        HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
        connection.setRequestProperty("Ocp-Apim-Subscription-Key", subscriptionKey);

        // receive JSON body
        InputStream stream = connection.getInputStream();
        String response = new Scanner(stream).useDelimiter("\\A").next();

        // construct result object for return
        SearchResults results = new SearchResults(new HashMap<String, String>(), response);

        // extract Bing-related HTTP headers
        Map<String, List<String>> headers = connection.getHeaderFields();
        for (String header : headers.keySet()) {
            if (header == null) continue;      // may have null key
           // if (header.startsWith("BingAPIs-") || header.startsWith("X-MSEdge-")) {
                results.relevantHeaders.put(header, headers.get(header).get(0));
           // }
        }

        stream.close();
        return results;
    }

    // pretty-printer for JSON; uses GSON parser to parse and re-serialize
    public static String prettify(String json_text) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(json_text).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(json);
    }

    public static String[] getSearchURLs(String jsonString) {
        JSONObject obj = new JSONObject(jsonString);
        //Get only webPages object from all Json
        JSONObject webPages = obj.getJSONObject("webPages");
        JSONArray arr = webPages.getJSONArray("value");
        //Bing returns first 10 serch results on the first page
        String[] urls = new String[10];
        for (int i = 0; i < arr.length(); i++) {
        urls[i] = arr.getJSONObject(i).getString("url");
        }
        return urls;
    }
    private static void coming(String URL, int depth, int urlId) {
        //Get domain word
        String domain = "";
        if (URL.contains(".")){
        String string =URL;
        String[] parts = string.split(Pattern.quote("."));
        domain = parts[1];}
        //Exclude domain
        if ((!visited.contains(URL) && (depth < max_depth))) {
            count++;
            System.out.println(">> Depth: " + depth + " [" + URL + "] visited = " + count + " URL ID = "+ urlId);
            visited.add(URL);

            Elements PageLinks = null;
            try {

                Document document = Jsoup.connect(URL).get();
                PageLinks = document.select("a[href]");
            } catch (Exception e) {
                System.err.println(e);
            }

            depth++;
            if (PageLinks != null) {
                for (Element page : PageLinks) {
                    if (!page.attr("abs:href").contains(domain))
                    coming(page.attr("abs:href"), depth, urlId);
                }
            }
        }
    }

    public static void main (String[] args) {
        if (subscriptionKey.length() != 32) {
            System.out.println("Invalid Bing Search API subscription key!");
            System.out.println("Please paste yours into the source code.");
            System.exit(1);
        }

        try {
            System.out.println("Searching the Web for: " + searchTerm);

            SearchResults result = SearchWeb(searchTerm);

            System.out.println("\nRelevant HTTP Headers:\n");
            for (String header : result.relevantHeaders.keySet())
                System.out.println(header + ": " + result.relevantHeaders.get(header));

            System.out.println("\nJSON Response:\n");
            String[] urls = getSearchURLs(result.jsonResponse);

            //Here we start to crawl
            Thread[] threads = new Thread[10];
            for (int i= 0; i < 10; i++) {
                int finalI = i;
                threads[i] = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        coming(urls[finalI], 0, finalI);
                    }
                });
            }

            for (int i= 0; i < 10; i++) {
                threads[i].run();
            }


        }
        catch (Exception e) {
            e.printStackTrace(System.out);
            System.exit(1);
        }
    }
}


